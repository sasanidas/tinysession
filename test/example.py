from flask import Flask, request
import flask_mysqldb
from tinysession import TSession
import hashlib


app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_PORT'] = 3306
app.config['MYSQL_DB'] = 'anyDB'
app.config['MYSQL_USER'] = 'YOUR-USER'
app.config['MYSQL_PASSWORD'] = 'YOUR-PASSWORD'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
app.config['SECRET_KEY'] = 'CREATE-A-SECRET-KEY'
mysql = flask_mysqldb.MySQL(app)

#---------------- Estas 3 funciones deben estar en tu codigo. La segunda(2) y la tercera(3)
#---------------- puedes dejarlas tal cual, la primera(1-sessionTrigger) la puede modificar

#1-CALLBACK FUNCTION
def sessionTrigger(sessionKeys):
    """
    Este metodo es llamado por tinysession de forma automatica si la sesion caduca.
    Le retornara todas las claves, la sesion sera destruida y usted podra hacer lo que
    entienda necesario con las claves que se le entregan y su base de datos.
    """

    utoken = sessionKeys['__sessionToken']
    utoken2 = sessionKeys['myToken']
    
    cur = mysql.connection.cursor()
    cur.execute("UPDATE user SET userState=0 WHERE userToken = %(userToken)s",{'userToken':utoken2})
    curFetch = cur.fetchall()
    cur.close()

#2-CONTROL FUNCTION
#@app.route............. puedes crear rutas para pruebas
def TinySession(operation, key='', value='', destroyIfTimeout=1, privateMethod=False):
    """
    Configuracion basica para acceder a los metodos de la libreria.Usted es libre de modificar este codigo
    como guste.
    """
    
    DEFAULT_KEYS = {'myToken':None}
    SESSION_TIMEOUT = 10
    SESSION_TRIGGER = sessionTrigger
    tns = TSession('tinyDB', request, DEFAULT_KEYS, SESSION_TIMEOUT, SESSION_TRIGGER)
    if operation == 'createSession':
        tns(key)
    elif operation == 'destroySession':
        tns.destroySession()
    elif operation == 'addKey':
        tns.addKey(key, value, False)
    elif operation == 'getKey':
        tns.getKey(key, False)
    elif operation == 'setKey':
        tns.setKey(key, value, False)
    elif operation == 'isTimedOut':
        tns.isTimedOut(destroyIfTimeout)
    else:
        raise ValueError("La operacion indicada no es valida")
    return tns.tnsresult

#3-CHECKTIMEOUT FUNCTION
#@app.route............. puedes crear rutas para pruebas
def isTimedOut(destroyIfTimeout=1):
    """
    Accede a la sesion actual y retorna informacion con respecto al estado de la sesion.
    destroyIfTimeout=1 indica que si la sesion esta sobrepasada en tiempo de inactividad el sistema
    destruira la sesion.
    """
    
    return TinySession('isTimedOut', '', '', destroyIfTimeout)


#--------------------- EL FLUJO COMUN DE SU APLICACION ----------------------------

@app.route('/api/sha1/<string:value>')
def generateSHA(value):
    """
    Generar un codigo SHA1
    """

    return hashlib.sha1(value.encode()).hexdigest()

@app.route('/api/login')
def login():
    """
    Ejemplo de un escenario de LOGIN
    """

    token = 'CREATE-A-TOKEN'
    #Inicializa una sesion y le pasa un token.Solo es necesario inicializar una unica ves.
    TinySession('createSession', token)
    
    #Modifica una clave,en este caso esta clave se inicializo junto a la sesion
    TinySession('setKey', 'myToken', token)
    
@app.route('/api/addMyKey')
def createNewKey():
    TinySession('addKey', 'userID', 123)

@app.route('/api/getMyKey')
def getMyKey(key):
    return TinySession('getKey', key)['userID']


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
